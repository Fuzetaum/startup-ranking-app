import {
  Avatar,
  Card,
  Divider,
  Spin,
} from 'antd';
import React from 'react';
import { withRouter } from 'react-router-dom';

import { getStartupList, getSegmentById } from '../../../actions/startupEndpointActions';

import './index.css';

class StartupDescription extends React.Component {
  state = {
    isLoading: true,
    startup: {},
    segment: '',
  }

  async componentDidMount() {
    const { match } = this.props;
    const startups = await getStartupList();

    const startup = startups.find(startup => startup.name === match.params.name);

    const segment = await getSegmentById(startup.segment_id);

    console.log(startup);
    console.log(segment);

    this.setState({ startup, segment, isLoading: false });
  }

  render() {
    const { startup, segment, isLoading } = this.state;

    if (isLoading) {
      return (
        <div className="startup-description-card">
          <Spin />
        </div>
      )
    }

    return (
      <Card
        className="startup-description-card"
        bodyStyle={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <Avatar src={startup.imageUrl} style={{ width: '50%', height: 'auto' }} />
        <span className="startup-description-name">{startup.name}</span>
        <span className="startup-description-segment">{segment.name}</span>
        <Divider />
        <span className="startup-description">{startup.description}</span>
      </Card>
    );
  }
}

export default withRouter(StartupDescription);
