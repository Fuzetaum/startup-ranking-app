import React from 'react';

const NotFound = () => (
  <div>
    <span>A página que você procura não existe... :(</span>
  </div>
)

export default NotFound;
