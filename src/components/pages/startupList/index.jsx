import { Avatar, Card, Spin } from 'antd';
import React from 'react';
import { withRouter } from 'react-router-dom';

import { getStartupList } from '../../../actions/startupEndpointActions';

import './index.css';

class StartupList extends React.Component {
  state = {
    isLoading: true,
    startupList: [],
  }

  async componentDidMount() {
    const startups = await getStartupList();

    this.setState({ startupList: startups, isLoading: false });
  }

  render() {
    const { startupList, isLoading } = this.state;

    if (isLoading) {
      return (
        <div className="startup-list-card">
          <Spin />
        </div>
      )
    }

    return (
      <div className="startup-list-card">
        {/* <Card
          style={{ width: '96%' }}
          bodyStyle={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            backgroundColor: '#fafafa',
          }}
        > */}
          {!startupList.length ?
            <span>Não existem StartUps pra mostrar... :(</span>
            : startupList.map(startup => (
              <Card className="startup-list-item-card"
                bodyStyle={{
                  display: 'flex',
                  flexDirection: 'column',
                  alignItems: 'center',
                }}
                onClick={() => { this.props.history.push(`/startup/${startup.name}`); }}
              >
                <Avatar src={startup.imageUrl} style={{ width: '50%', height: 'auto' }} />
                <span className="startup-list-item-name">{startup.name}</span>
                <span className="startup-list-item-segment">{startup.Segment.name}</span>
              </Card>
            ))
          }
        {/* </Card> */}
      </div>
    );
  }
}

export default withRouter(StartupList);
