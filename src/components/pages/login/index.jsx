import { Button } from 'antd';
import React from 'react';
import { withRouter } from 'react-router-dom';

class Login extends React.Component {
  state = {}

  render() {
    return (
      <div>
        <Button onClick={() => { this.props.history.push('/list'); }}>Conheça as StartUps!</Button>
      </div>
    );
  }
}

export default withRouter(Login);