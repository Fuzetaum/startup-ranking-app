import React from 'react';
import { Route } from 'react-router-dom';

import Layout from '../layout';

const LayoutRoute = ({ component, ...rest }) => (
  <Route {...rest}>
    <Layout contentComponent={component} />
  </Route>
);

export default LayoutRoute;
