import React from 'react';
import { Switch } from 'react-router-dom';

import Route from './LayoutRoute';

import Login from '../pages/login';
import StartupList from '../pages/startupList';
import StartupDescription from '../pages/startupDescription';
import NotFound from '../pages/notfound';

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/" component={Login} />

      <Route exact path="/list" component={StartupList} />
      <Route exact path="/startup/:name" component={StartupDescription} />

      <Route path="*" component={NotFound} />
    </Switch>
  );
}

export default Routes;
