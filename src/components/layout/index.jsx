import { Layout as AntLayout } from 'antd';
import React from 'react';

import Header from './Header';

const { Content } = AntLayout;

const Layout = ({ contentComponent: ContentComponent }) => {
  return (
    <AntLayout
    style={{ height: '100vh' }}
      hasSider={false}
    >
      <Header />
      <Content style={{ overflow: 'scroll' }}>
        <ContentComponent />
      </Content>
    </AntLayout>
  );
}

export default Layout;
