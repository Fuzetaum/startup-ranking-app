import {
  Layout as AntLayout,
  Button,
  Dropdown,
  Icon,
  Menu,
  Modal,
} from 'antd';
import React from 'react';
import { withRouter } from 'react-router-dom';

import { logout } from '../../actions/sessionActions';

import './Header.css';

const { Header: AntHeader } = AntLayout;

class Header extends React.Component {
  state = {
    showModal: false,
  }

  handleDropdownClick = ({ key }) => {
    switch (key) {
      case 'logout':
        this.setState({ showModal: true });
      case 'list-all':
        this.props.history.push('/list');
    }
  }

  handleLogout = () => {
    logout(this.props.history);
    this.handleShowModal();
  }

  handleShowModal = () => {
    this.setState({ showModal: !this.state.showModal });
  }

  menu = (
    <Menu onClick={this.handleDropdownClick}>
      <Menu.Item key="list-all">Startups</Menu.Item>
      <Menu.Divider />
      <Menu.Item key="logout"><Icon type="poweroff" /><span>Logout</span></Menu.Item>
    </Menu>
  )

  render() {
    const { showModal } = this.state;

    return (
      <AntHeader className="header-container">
        <Dropdown overlay={this.menu} trigger={['click']} className="header-dropdown">
            <Icon type="profile" className="header-button-icon" />
        </Dropdown>
        <span className="header-title">The StartUp Festival App</span>
        <Modal
          visible={showModal}
          onOk={this.handleLogout}
          onCancel={this.handleShowModal}
        >
          <span>Você deseja mesmo sair?</span>
        </Modal>
      </AntHeader>
    );
  }
}

export default withRouter(Header);
