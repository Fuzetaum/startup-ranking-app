import request from 'superagent';

export async function post(url, params){
  try {
      const result = await request
      .post(url)
      .set('Content-Type', 'application/json')
      .send(JSON.stringify(params));
      return {
          data: result,
          error: null,
      };
  }catch(err){
      return {
          data: null,
          error: err,
      };
  }
}
