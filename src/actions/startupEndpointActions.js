import { BACKEND_URL } from '../constants';
import { post } from './requestTemplate';

export const getStartupList = async () => {
  const response = await post(BACKEND_URL, {
    query: '{allStartups{name,imageUrl,segment_id,teamCount,annualReceipt,description,Segment{name}}}'
  });

  return response.data.body.data.allStartups;
}

export const getSegmentList = async () => {
  const response = await post(BACKEND_URL, {
    query: '{allSegments{id,name}}'
  });

  return response.data.body.data.allSegments;
}

export const getSegmentById = async (id) => {
  const response = await post(BACKEND_URL, {
    query: `{Segment(id:${id}){name}}`
  });

  return response.data.body.data.Segment;
}
